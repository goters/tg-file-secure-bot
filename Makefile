APP = tg-file-secure-bot
CGO_ENABLED ?= 0
GOOS ?= linux
REGISTRY = registry.gitlab.com

test-unit:## Run unittest
	@go test  ./... -coverprofile .testCoverage.txt
	@go tool cover -func=.testCoverage.txt

build:
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(GOOS) GOARCH=$(GOARCH) GOARM=$(GOARM) go build -ldflags "-s -w" -o ./bin/${APP} ./cmd/auth-server/*.go

lint:
	goimports -d -w $$(find ./ -type f -name '*.go' -not -path './vendor/*')
	golangci-lint run ./...